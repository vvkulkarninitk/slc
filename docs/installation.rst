============
Installation
============

At the command line::

    $ easy_install slc

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv slc
    $ pip install slc
