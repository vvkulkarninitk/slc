===============================
slc
===============================

.. image:: https://badge.fury.io/py/slc.png
    :target: http://badge.fury.io/py/slc

.. image:: https://travis-ci.org/viveksck/slc.png?branch=master
        :target: https://travis-ci.org/viveksck/slc

.. image:: https://pypip.in/d/slc/badge.png
        :target: https://pypi.python.org/pypi/slc


Code for statistically significant linguistic change.

* Free software: BSD license
* Documentation: https://slc.readthedocs.org.

Features
--------

* TODO
